###  A sample logster parser file that can be used to count the number
###  of response codes found in an Apache access log.
###
###  For example:
###  sudo ./logster --dry-run --output=ganglia SampleLogster /var/log/httpd/access_log
###
###
###  Copyright 2011, Etsy, Inc.
###
###  This file is part of Logster.
###
###  Logster is free software: you can redistribute it and/or modify
###  it under the terms of the GNU General Public License as published by
###  the Free Software Foundation, either version 3 of the License, or
###  (at your option) any later version.
###
###  Logster is distributed in the hope that it will be useful,
###  but WITHOUT ANY WARRANTY; without even the implied warranty of
###  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
###  GNU General Public License for more details.
###
###  You should have received a copy of the GNU General Public License
###  along with Logster. If not, see <http://www.gnu.org/licenses/>.
###

import time
import re

from logster.logster_helper import MetricObject, LogsterParser
from logster.logster_helper import LogsterParsingException

class ApacheLogster(LogsterParser):

    def __init__(self, option_string=None):
        '''Initialize any data structures or variables needed for keeping track
        of the tasty bits we find in the log we are parsing.'''
        self.status_1xx = 0
        self.status_2xx = 0
        self.status_3xx = 0
        self.status_4xx = 0
        self.status_5xx = 0
        self.response_time = 0
        
        # Regular expression for matching lines we are interested in, and capturing
        # fields from the line (in this case, http_status_code).
        self.reg = re.compile('msec=(?P<msec>\d{1,})\|status=(?P<http_status_code>\d{3})')


    def parse_line(self, line):
        '''This function should digest the contents of one line at a time, updating
        object's state variables. Takes a single argument, the line to be parsed.'''

        try:
            # Apply regular expression to each line and extract interesting bits.
            regMatch = self.reg.match(line)

            if regMatch:
                linebits = regMatch.groupdict()
                status = int(linebits['http_status_code'])

                self.response_time = int(linebits['msec']) * 0.001

                if (status < 200):
                    self.status_1xx += 1
                elif (status < 300):
                    self.status_2xx += 1
                elif (status < 400):
                    self.status_3xx += 1
                elif (status < 500):
                    self.status_4xx += 1
                else:
                    self.status_5xx += 1

            else:
                raise LogsterParsingException, "regmatch failed to match"

        except Exception, e:
            raise LogsterParsingException, "regmatch or contents failed with %s" % e


    def get_state(self, duration):
        '''Run any necessary calculations on the data collected from the logs
        and return a list of metric objects.'''
        self.duration = duration
        
        metrics = [
            MetricObject("apache.status_1xx", self.status_1xx, "request", "int", metric_type="c"),
            MetricObject("apache.status_2xx", self.status_2xx, "request", "int", metric_type="c"),
            MetricObject("apache.status_3xx", self.status_3xx, "request", "int", metric_type="c"),
            MetricObject("apache.status_4xx", self.status_4xx, "request", "int", metric_type="c"),
            MetricObject("apache.status_5xx", self.status_5xx, "request", "int", metric_type="c"),
        ]

        if (self.response_time > 0):
            metrics.append(MetricObject("apache.response_time", self.response_time, "ms", "int", metric_type="ms"))

        return metrics
